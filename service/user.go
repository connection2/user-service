package service

import (
	"context"
	pbp "temp/genproto/book"
	pb "temp/genproto/userr"
	"temp/pkg/logger"
	"temp/storage"
	"temp/service/client"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"	
)

type UserRService struct{
	storage storage.IStorage
	logger logger.Logger
	client client.GrpcClientI
}

func NewUserRService(db *sqlx.DB, log logger.Logger, client client.GrpcClientI) *UserRService{
	return &UserRService{
		storage: storage.NewStoragePg(db),
		logger: log,
		client: client,
	}
}

func (u *UserRService) CreateUserR(ctx context.Context, req *pb.Userr) (*pb.Userr, error){
	user, err := u.storage.UserR().CreateUserR(req)
	if err != nil{
		u.logger.Error("Error with UserR",logger.Any("error insert UserR",err))
		return &pb.Userr{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	return user, nil
}

func (u *UserRService) GetUserR(ctx context.Context, req *pb.UserrID) (*pb.Userr, error) {
	user, err := u.storage.UserR().GetUserR(req.Id)
	if err != nil{
		u.logger.Error("Error with UserR",logger.Any("error insert UserR",err))
		return &pb.Userr{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}	
	return user, nil
}

func (u *UserRService) ByBook(ctx context.Context, req *pb.ByBookRequest) (*pb.ByBookResponse, error) {
	user, err := u.storage.UserR().ByBook(req)
	if err != nil{
		u.logger.Error("Error with UseRR",logger.Any("error insert UseRR",err))
		return &pb.ByBookResponse{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	book, err := u.client.Book().GetBookID(ctx, &pbp.GetBook{
		Id: req.BookId,
	})
	if err != nil{
		u.logger.Error("Error with  Book",logger.Any("error insert  Book",err))
		return &pb.ByBookResponse{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}

	responce, err := u.storage.UserR().ByBook(req)
	if err != nil{
		u.logger.Error("Error with RESPONCE",logger.Any("error insert RESPONCE",err))
		return &pb.ByBookResponse{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	

}