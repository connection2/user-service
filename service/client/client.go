package client

import (
	"fmt"
	"temp/config"
	pbp "temp/genproto/book"

	"google.golang.org/grpc"
)

//GrpcClientI ...
type GrpcClientI interface{
	Book() pbp.BookServiceClient
}
//GrpcClient ...
type GrpcClient struct{
	cfg config.Config
	bookService pbp.BookServiceClient
}

//New ...
func New(cfg config.Config) (*GrpcClient, error){
	connBook, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.BookServiceHost,cfg.BookServicePort),
		grpc.WithInsecure())
	if err != nil{
		return nil, fmt.Errorf("post service dial host: %s port: %d",
	cfg.BookServiceHost,cfg.BookServicePort)
	}
	return &GrpcClient{
		cfg: cfg,
		bookService: pbp.NewBookServiceClient(connBook),	
	},nil
}

func (r *GrpcClient) Book() pbp.BookServiceClient{
	return r.bookService
}