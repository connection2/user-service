package storage

import (
	"temp/storage/postgres"
	"temp/storage/repo"
	"github.com/jmoiron/sqlx"
)

type IStorage interface{
	UserR() repo.UserRStorage
}
type storagePg struct{
	db *sqlx.DB
	userRRepo repo.UserRStorage
}
func NewStoragePg(db *sqlx.DB) *storagePg{
	return &storagePg{
		db: db,
		userRRepo: postgres.NewUserRepo(db),
	}	
}

func (s storagePg) UserR() repo.UserRStorage{
	return s.userRRepo
}