package repo

import pb "temp/genproto/userr"

type UserRStorage interface{
	CreateUserR(*pb.Userr) (*pb.Userr, error)
	GetUserR(userID int64) (*pb.Userr, error)
	ByBook(*pb.ByBookRequest) (*pb.ByBookResponse, error)
}