package postgres

import (
	
	pb "temp/genproto/userr"

	"github.com/jmoiron/sqlx"
)

type userRepo struct{
	db *sqlx.DB
}

func NewUserRepo(db *sqlx.DB) *userRepo{
	return &userRepo{db: db}
}

func (u *userRepo) CreateUserR(req *pb.Userr) (*pb.Userr, error) {

	user := pb.Userr{}
	err := u.db.QueryRow(`insert into users(name, balance) values($1, $2) 
	returning id, name, balance`,req.Name, req.Balance).Scan(&user.Id, &user.Name, &user.Balance)
	if err != nil{
		return &pb.Userr{}, err
	}
	return &user, nil
}

func (u *userRepo) GetUserR(userID int64) (*pb.Userr, error) {
	user := pb.Userr{}
	err := u.db.QueryRow(`select id, name, balance where id = $1`,userID).Scan(&user.Id, &user.Name, &user.Balance)
	if err != nil{
		return &pb.Userr{}, err
	}
	return &user, nil
}

func (u *userRepo) ByBook(req *pb.ByBookRequest) (*pb.ByBookResponse, error) {

	books := pb.ByBookResponse{}
	err := u.db.QueryRow(`insert into user_book(owner_id, book_id, amount) values($1, $2, $3) 
	returning owner_id, book_id, amount`,req.OwnerId, req.BookId, req.Amount).Scan(&books.Id, &books.Name, &books.Balance)
	if err != nil{
		return &pb.ByBookResponse{}, err
	}

	update, err := u.db.Exec(`update users SET balance = balance - $1 where book_id = $2`,books.Balance, req.BookId)
	if err != nil{
		return &pb.ByBookResponse{}, err
	}

	

	return &books, nil

}